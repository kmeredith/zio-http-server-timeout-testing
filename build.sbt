scalaVersion := "2.13.10"

libraryDependencies ++= Seq(
  "org.tpolecat" %% "doobie-core" % "0.13.4",
  "org.tpolecat" %% "doobie-postgres" % "0.13.4",
  "dev.zio" %% "zio-http" % "0.0.4",
  "dev.zio" %% "zio-interop-cats" % "2.5.1.1"
)
