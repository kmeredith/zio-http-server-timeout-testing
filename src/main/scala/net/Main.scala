package net

import zio.http._
import zio.http.model._
import zio.http.Server
import zio._
import zio.Clock.ClockJava
import zio.interop.catz._
import zio.http.middleware.RequestHandlerMiddlewares

object HelloWorldWithCORS extends ZIOAppDefault {

  val app: HttpApp[Clock, Nothing] =
      Http.collectZIO[Request] {
        case Method.GET -> !! / "text" => ZIO.sleep(10.seconds).as(Response.text("Hello World!"))
        case Method.GET -> !! / "json" => {
          ZIO.runtime.flatMap { implicit r: Runtime[Clock] =>
            Repository.result[Task]
          }.as(Response.json("""{"greetings": "Hello World!"}"""))
        }.tapEither(a => ZIO.succeed(println(a.toString))).orDie
      } @@ RequestHandlerMiddlewares.timeout(5.seconds)

  override val run =
    Server.serve(app).provide(Server.default ++ ZLayer.succeed(new ClockJava(java.time.Clock.systemUTC())))
}