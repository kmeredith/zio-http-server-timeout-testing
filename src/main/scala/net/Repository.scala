package net

import cats._
import cats.effect._
import cats.implicits._
import scala.concurrent.ExecutionContext
import doobie.util.ExecutionContexts
import doobie._
import doobie.implicits._

object Repository {

  implicit val cs: ContextShift[IO] = cats.effect.IO.contextShift(ExecutionContext.global)

  def xa[F[_]](implicit A : Async[F], CS: ContextShift[F])= Transactor.fromDriverManager[F](
    "org.postgresql.Driver", // driver classname
    "jdbc:postgresql:postgres", // connect URL (driver-specific)
    "postgres", // user
    "postgres" // password
  )

  // set statement_timeout to '10000'

  val x = {
    //sql"""select pg_sleep(3)"""
    sql"""UPDATE x SET a = 10 where y = 'foo' RETURNING a"""
  }

  def result[F[_]](implicit A : Async[F], CS: ContextShift[F]): F[Long] = x.query[Long].unique.transact[F](xa)

}
